# Spring Boot/MVC - CRUD API Test

## Overview

This application provides a simple API to CRUD payment records. Upon start-up, it loads a number of default payment records from an inlined JSON file resource, in to a MongoDB running on localhost:27017.

The back-end is written in Java 8 and based on Spring Boot/MVC framework.

Dynamic API documentation is provided via Swagger UI and REST API metrics/health via Spring Actuator. Security has been enforced using Spring security BASIC HTTP authentication.

Please see the design documentation PDF for further information.

## Build Environment

### Clean Build (Vagrant)

This is not required to build the project, but it provides a clean isolated environment to do so utilising Vagrant & VirtualBox.
The `Vagrantfile` is configured to build a VM based on CentOS 7, containing OpenJDK-devel 8, Maven, Docker and git.

- Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) (currently v5.2.2)
- Install [Vagrant x64](https://www.vagrantup.com/downloads.html) (currently v2.0.2). Windows 7 hosts require WMF 5.1
- Add vagrant to the environment PATH. On Windows, this is typically: C:\HashiCorp\Vagrant\bin, e.g from an admin DOS cmd: `setx /M PATH "%PATH%;C:\HashiCorp\Vagrant\bin"`
- Install vagrant plugins: `vagrant plugin install vagrant-vbguest`
- Open up a terminal to the source code root and run: `vagrant up`
- To connect to the VM: `vagrant ssh`
- Enter `cd /vagrant` to drop to source code root. This is mapped to the source root directory on the host.
- Proceed with the build process below (see: Building)

### Linux/Other

Ensure the following are installed:

- Java JDK 8 (e.g openjdk-devel)
- Maven 3.5.x

## Building

From the source root, to build run: `mvn clean install`. This will compile, test and package the application which can then be run stand-alone (see: Running). The output is a jar (~50MB) -> `./target/crudtestf3-*.jar`
To optionally override MongoDB settings for the integration tests to run against, specify the following arguments, e.g: 

```
mvn clean install -Dspring.data.mongodb.host=192.168.1.12 -Dspring.data.mongodb.username=admin -Dspring.data.mongodb.password=newpass
```

## Running

### Pre-Requisites

- Java 8 – JRE or JDK
- MongoDB – Running on localhost:27017 with user/password authentication set-up (see: Configuration -> spring.data.mongodb.*). The Vagrant build environment already provides this.
- TCP port 8080 open and free

### Launching

To run as a Stand-alone Java application:

1. Change to `./target`
1. Start the application with: `java -jar crudtestf3-*.jar`. Optionally add configuration overrides to the arguments list (see: Configuration)

### Logging

- Logs: `./logs/crudtestf3.log`
- Default Level: INFO 
- File: ./logs/crudtestf3.log
- Max size: 10MiB
- History: 10 files

## Configuration

The following properties are set by a default application.properties file built in to the application:

**server.port** - HTTP port listener (default: 8080)

**spring.data.mongodb.host** - DB host (default: localhost)

**spring.data.mongodb.port** - DB port (default: 27017)

**spring.data.mongodb.database** - DB name (default: admin)

**spring.data.mongodb.username** - DB username (default: admin)

**spring.data.mongodb.password** - DB password (default: crudtestf3db)


Each property can be overridden by specifying it as a java property when running it from the command line , e.g to set a different mongodb host:

```
java -jar crudtestf3-*.jar --spring.data.mongodb.host=10.0.12.6 --spring.data.mongodb.username=admin --spring.data.mongodb.password=somepassword
```

## Using Curl with the API

To create a sample payment record using curl:
```
curl -v -H "Content-Type: application/json" -u admin:crudtestf3 -X GET http://localhost:8080/api/records/

curl -v -H "Content-Type: application/json" -u admin:crudtestf3 -X GET http://localhost:8080/api/records/216d4da9-e59a-4cc6-8df3-3da6e7580b77

curl -v -H "Content-Type: application/json" -u admin:crudtestf3 -X POST http://localhost:8080/api/records -d "@./src/test/resources/data/singlePayment.json"  

curl -v -H "Content-Type: application/json" -u admin:crudtestf3 -X DELETE http://localhost:8080/api/records/1ee3a8d8-ca7b-4290-a52c-dd5b6165ec43
```

For other API calls, see the design documentation PDF.

## Developing

### Back-end

This has been built using the [Spring Boot MVC Initializr](https://start.spring.io) targetting Java 8 & Spring Boot 1.5.9. 
A number of integration tests have been implemented to validate that the REST API methods function correctly.

The project has been configured to:

- Include Jetty instead of Tomcat
- Use Spring security HTTP BASIC authentication (hard coded credentials currently)
- Provide a self documenting API using swagger (see: /swagger-ui.html)
- Provide API metrics using Spring Actuator (see: /health & /metrics)
- Log using Logback
- Provide a CRUD endpoint for records (./records) which currently supports: POST, PUT, GET, DELETE
- A custom error controller (/error) for generating server side errors as JSON.

The project uses Lombok for the domain POJOs to cut down on getter/setter verbosity and to implement builder pattern without all the boilerplate.
The 'Record' entity extends from a base 'Record' object which contain common fields. There is future scope to add additional entity types. The integration tests have also been built with this in mind.
 
To develop the back-end code, add [Lombok](https://projectlombok.org) to Eclipse and import the code as a maven project.

To run the code in development mode e.g: `mvn spring-boot:run -Drun.jvmArguments='-Dserver.port=8081'`. 
This will run spring boot on the test port, with live-reload for certain aspects of the code (excluding domain object and dependency changes).

## Futures

### Back-end

- Dockerfile & docker compose configuration
- Implement checkstyle/PMD
- Enforce higher Jacoco test code coverage
- Batch update API for bulk loads of data
- OAuth2 authentication mechanism
- Additional roles (e.g readonly, recordAdmin, etc...)
- Different DB support
- Implement websockets so clients can register for changes to DB entities
- Search records API

## Useful Links

Spring Boot Initializr: [https://start.spring.io/]()

Spring Boot Swagger: [http://www.baeldung.com/swagger-2-documentation-for-spring-rest-api]()

Spring Boot Jetty Configuration: [https://docs.spring.io/spring-boot/docs/current/reference/html/howto-embedded-servlet-containers.html]()

Spring Boot Testing: [https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html]()

Spring + MongoDB: [https://spring.io/guides/gs/accessing-data-mongodb/]()

Eclipse FindBugs plugin: [http://www.vogella.com/tutorials/Findbugs/article.html]()

Project Lombok: [https://projectlombok.org]()