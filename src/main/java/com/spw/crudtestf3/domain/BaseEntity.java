package com.spw.crudtestf3.domain;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.jxpath.JXPathContext;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class BaseEntity {

    private JXPathContext jxContext;
    
    @JsonIgnore
    Map<String, Object> _links;

    /**
     * Set the a value on the current object using JXPath    
     * @param path JXPath e.g: /attributes/amount
     * @param value Any value
     */
    public void setValue(String path, Object value) {
        JXPathContext context = getJxContext();
        context.setValue(path, value);
    }

    private JXPathContext getJxContext() {
        if (jxContext != null) {
            return jxContext;
        }
        jxContext = JXPathContext.newContext(this);
        return jxContext;
    }
}
