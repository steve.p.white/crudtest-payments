package com.spw.crudtestf3.domain;

public class SizeConstants {
    public static final int AMOUNT = 32;
    public static final int ACCOUNT_NUMBER = 64;
    public static final int BANK_ID = 16;
    public static final int BANK_ID_CODE = 8;
}
