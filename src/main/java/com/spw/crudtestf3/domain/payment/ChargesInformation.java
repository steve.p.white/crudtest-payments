package com.spw.crudtestf3.domain.payment;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.spw.crudtestf3.domain.Currency;
import com.spw.crudtestf3.domain.SizeConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Builder
public class ChargesInformation {

    @NotEmpty
    @Size(max = 16)
    String bearer_code;
    
    @NotEmpty
    List<CurrencyAmount> sender_charges;
    
    @NotEmpty
    @Size(max = SizeConstants.AMOUNT)
    String receiver_charges_amount;
    
    @NotNull
    Currency receiver_charges_currency;
}
