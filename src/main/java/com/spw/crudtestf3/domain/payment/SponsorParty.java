package com.spw.crudtestf3.domain.payment;

import javax.validation.constraints.Size;

import com.spw.crudtestf3.domain.SizeConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Builder
public class SponsorParty {
    @NotEmpty
    @Size(max = SizeConstants.ACCOUNT_NUMBER)
    String account_number;
    
    @NotEmpty
    @Size(max = SizeConstants.BANK_ID)
    String bank_id;
    
    @NotEmpty
    @Size(max = SizeConstants.BANK_ID_CODE)
    String bank_id_code;
}
