package com.spw.crudtestf3.domain.payment;

import javax.validation.constraints.Size;

import com.spw.crudtestf3.domain.SizeConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Builder
public class Party {
    @NotEmpty
    @Size(max = 64)
    String account_name;
    
    @NotEmpty
    @Size(max = SizeConstants.ACCOUNT_NUMBER)
    String account_number;
    
    @NotEmpty
    @Size(max = 8)
    String account_number_code;
    
    int account_type;
    
    @NotEmpty
    @Size(max = 128)
    String address;
    
    @NotEmpty
    @Size(max = SizeConstants.BANK_ID)
    String bank_id;
    
    @NotEmpty
    @Size(max = SizeConstants.BANK_ID_CODE)
    String bank_id_code;
    
    @NotEmpty
    @Size(max = 32)
    String name;
}
