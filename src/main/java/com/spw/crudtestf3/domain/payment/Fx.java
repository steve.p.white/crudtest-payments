package com.spw.crudtestf3.domain.payment;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.spw.crudtestf3.domain.Currency;
import com.spw.crudtestf3.domain.SizeConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Builder
public class Fx {
    @NotEmpty
    @Size(max = 16)
    String contract_reference;
    
    @NotEmpty
    @Size(max = 16)
    String exchange_rate;
    
    @NotEmpty
    @Size(max = SizeConstants.AMOUNT)
    String original_amount;
    
    @NotNull
    Currency original_currency;
}
