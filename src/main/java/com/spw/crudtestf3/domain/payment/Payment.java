package com.spw.crudtestf3.domain.payment;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spw.crudtestf3.domain.Currency;
import com.spw.crudtestf3.domain.SizeConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Builder
public class Payment {

    private static final ObjectMapper mapper = new ObjectMapper();

    @NotEmpty
    @Size(max = SizeConstants.AMOUNT)
    String amount;

    @NotNull
    @Valid
    Party beneficiary_party;

    @NotNull
    @Valid
    ChargesInformation charges_information;

    @NotNull
    @Valid
    Currency currency;

    @NotNull
    @Valid
    Party debtor_party;

    @NotEmpty
    @Size(max = 64)
    String end_to_end_reference;

    @NotNull
    @Valid
    Fx fx;

    @NotEmpty
    @Size(max = 32)
    String numeric_reference;

    @NotEmpty
    @Size(max = 32)
    String payment_id;

    @NotEmpty
    @Size(max = 32)
    String payment_purpose;

    @NotEmpty
    @Size(max = 16)
    String payment_scheme;

    @NotEmpty
    @Size(max = 16)
    String payment_type;

    @NotEmpty
    @Size(max = 10)
    String processing_date;

    @NotEmpty
    @Size(max = 64)
    String reference;

    @NotEmpty
    @Size(max = 32)
    String scheme_payment_sub_type;

    @NotEmpty
    @Size(max = 32)
    String scheme_payment_type;

    @Valid
    @NotNull
    SponsorParty sponsor_party;

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public Map<String, Object> toMap() {
        return mapper.convertValue(this, new TypeReference<Map<String, Object>>() {
        });
    }
}
