package com.spw.crudtestf3.domain;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;

public class RecordTypes {
    public static final String PAYMENT = "payment";
    
    public static final List<String> ALL =  Collections.unmodifiableList(Lists.newArrayList(PAYMENT));
}
