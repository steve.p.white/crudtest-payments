package com.spw.crudtestf3.domain;

public enum Currency {
	GBP,
	USD,
	CHF
}
