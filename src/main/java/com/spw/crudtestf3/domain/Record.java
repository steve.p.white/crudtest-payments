package com.spw.crudtestf3.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.google.common.collect.ImmutableList;
import com.spw.crudtestf3.Util;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Builder
@Document(collection = "records")
public class Record extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final ImmutableList<String> HEADERS = ImmutableList.of("id", "version", "timeAdded", "timeUpdated");

    @Id
    @NotEmpty
    @Pattern(regexp = Util.UUID_PATTERN, message = "TokenFormatError")
    String id;

    @NotNull
    Long version;
    
    @NotNull
    Long timeAdded;
    
    @NotNull
    Long timeUpdated;

    @NotEmpty
    String type;

    @NotEmpty
    @Pattern(regexp = Util.UUID_PATTERN, message = "TokenFormatError")
    String organisation_id;

    @Valid
    @NotEmpty
    Map<String, Object> attributes;

    public void setTimeUpdatedNow() {
        this.timeUpdated = new Date().getTime();
    }

    public void setNewRecord() {
        long timeNow = new Date().getTime();

        this.timeAdded = timeNow;
        this.timeUpdated = timeNow;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public boolean equalsRecord(Object other) throws Exception {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Record)) {
            return false;
        }
        Record that = (Record) other;

        return type.equals(that.type) &&
                id.equals(that.id) &&
                organisation_id.equals(that.organisation_id) &&
                Util.checkMapsAreIdentical(this.attributes, that.attributes) &&
                timeAdded.equals(that.timeAdded) &&
                timeUpdated.equals(that.timeUpdated);
    }

    public boolean equalsData(Object other) throws Exception {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Record)) {
            return false;
        }
        Record that = (Record) other;

        return type.equals(that.type) &&
                id.equals(that.id) &&
                organisation_id.equals(that.organisation_id) &&
                Util.checkMapsAreIdentical(attributes, that.attributes);
    }
}
