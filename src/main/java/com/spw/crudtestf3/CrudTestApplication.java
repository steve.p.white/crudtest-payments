package com.spw.crudtestf3;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.spw.crudtestf3.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.info.BuildProperties;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories("com.spw.crudtestf3.repositories")
public class CrudTestApplication {

    private static final Logger log = LoggerFactory.getLogger(CrudTestApplication.class);
    
    @Autowired
    BuildProperties buildProperties;

    Date startDate;
    
    public long getStartTime() {
        return this.startDate.getTime();
    }

    public static void main(String[] args) {
        SpringApplication.run(CrudTestApplication.class, args);
    }
    
    @PostConstruct
    public void init(){
        log.info("Starting: {}", Util.getVersion(buildProperties));
        this.startDate = new Date();
    }
    
    @PreDestroy
    public void close() {
        log.info("Shutdown...");
    }
}
