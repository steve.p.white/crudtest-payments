package com.spw.crudtestf3.controller;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spw.crudtestf3.Util;
import com.spw.crudtestf3.domain.Record;
import com.spw.crudtestf3.domain.RecordTypes;
import com.spw.crudtestf3.domain.payment.Payment;
import com.spw.crudtestf3.repositories.RecordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ExposesResourceFor(Record.class)
@BasePathAwareController
@RequestMapping(value = "/records")
@RepositoryRestController
public class RecordController {
    private static final Logger log = LoggerFactory.getLogger(RecordController.class);

    private final RecordRepository repository;

    private final static ObjectMapper mapper = new ObjectMapper();

    @Value("${api.debug}")
    private String debug;

    @Autowired
    public RecordController(RecordRepository repository,
            PagedResourcesAssembler<Object> pagedResourcesAssembler) {
        this.repository = repository;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE })
    public @ResponseBody PersistentEntityResource get(HttpServletRequest request, @PathVariable("id") String id,
                                                      PersistentEntityResourceAssembler resourceAssembler, HttpServletResponse response) {
        Util.logRequest(log, request);

        Record existingRecord = repository.findOne(id);
        if (existingRecord != null) {
            if (log.isTraceEnabled()) {
                log.trace("Found: {}", existingRecord);
            }

            return resourceAssembler.toResource(existingRecord);
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE })
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody PersistentEntityResource post(HttpServletRequest request, @RequestBody Map<String, Object> newEntityResource,
                                                       PersistentEntityResourceAssembler resourceAssembler, HttpServletResponse response)
            throws Exception {
        Util.logRequest(log, request);

        Record newRecord = mapper.convertValue(newEntityResource, Record.class);
        Record savedEntity = postRecord(newRecord);

        return resourceAssembler.toResource(savedEntity);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE })
    public @ResponseBody PersistentEntityResource put(HttpServletRequest request, @PathVariable("id") String id, @RequestBody Resource<Record> newEntityResource,
                                                      PersistentEntityResourceAssembler resourceAssembler, HttpServletResponse response)
            throws Exception {
        Util.logRequest(log, request);

        Record existingRecord = repository.findOne(id);
        if (existingRecord != null) {

            Record newRecord = newEntityResource.getContent();
            BeanUtils.copyProperties(newRecord, existingRecord, Record.HEADERS.toArray(new String[] {})); // ignore HEADER_FIELDS e.g: "timeAdded", "version", "id", ...

            validateRecord(existingRecord); // throws exception if invalid

            existingRecord.setTimeUpdatedNow();

            Record savedEntity = repository.save(existingRecord);
            response.setStatus(HttpServletResponse.SC_CREATED);
            return resourceAssembler.toResource(savedEntity);
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody PersistentEntityResource delete(HttpServletRequest request, @PathVariable("id") String id,
                                                         PersistentEntityResourceAssembler resourceAssembler, HttpServletResponse response) {
        Util.logRequest(log, request);
        repository.delete(id);
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);

        return null;
    }

    @RequestMapping(path = "/", method = RequestMethod.DELETE)
    public @ResponseBody PersistentEntityResource deleteAll(HttpServletRequest request,
                                                            PersistentEntityResourceAssembler resourceAssembler, HttpServletResponse response) {

        if (!getDebug()) { // Only allow DELETE all records when testing
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        repository.deleteAll();
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        return null;
    }

    private boolean getDebug() {
        if (this.debug == null) {
            return false;
        }
        if (!Boolean.parseBoolean(this.debug)) {
            return false;
        } else {
            return true;
        }
    }

    // TODO: We can do better here
    public static Object getInnerRecord(Record newRecord) {
        String recordTypeLower = newRecord.getType().toLowerCase();
        switch (recordTypeLower) { // Convert the Record -> Attributes into -> type (e.g Payment)
            case RecordTypes.PAYMENT:
                return mapper.convertValue(newRecord.getAttributes(), Payment.class);
            default:
                throw new IllegalArgumentException("Unknown record type: '" + newRecord.getType() + "'");
        }
    }

    public static void validateRecordType(Record newRecord) {
        String recordTypeLower = newRecord.getType().toLowerCase();
        if (!RecordTypes.ALL.contains(recordTypeLower)) {
            throw new IllegalArgumentException("Unknown record type: '" + newRecord.getType() + "'");
        }
    }

    public Record postRecord(Record newRecord) {

        newRecord.setNewRecord();
        validateRecord(newRecord); // throws exception if invalid

        return repository.save(newRecord);
    }

    public static void validateRecord(Record newRecord) {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        validateRecordType(newRecord);

        // Check Record constraints
        Set<ConstraintViolation<Object>> violations = validator.validate(newRecord);
        if (violations.size() > 0) {
            throw new DataIntegrityViolationException("Record constraint violations: " + violations.toString());
        }

        Object innerRecord = getInnerRecord(newRecord);
        // Check Record constraints (this recurses)
        violations = validator.validate((Payment) innerRecord);
        if (violations.size() > 0) {
            throw new DataIntegrityViolationException("Constraint violations: " + violations.toString());
        }
    }
}
