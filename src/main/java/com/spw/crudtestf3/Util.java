package com.spw.crudtestf3;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Maps;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.spw.crudtestf3.domain.Record;
import org.slf4j.Logger;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;

public class Util {

    public static final ListTypeRef<Record> JSONPATH_TYPEREF_LIST = new ListTypeRef<Record>() {
    };

    public static final TypeReference<List<Record>> JACKSON_TYPEREF_LIST = new TypeReference<List<Record>>() {
    };
    public static final TypeReference<Map<String, Object>> JACKSON_TYPEREF_MAP = new TypeReference<Map<String, Object>>() {
    };
    
    public static final String UUID_PATTERN = "^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$";

    public static final Configuration JSONPATH_CONF = Configuration
            .builder()
            .mappingProvider(new JacksonMappingProvider())
            .jsonProvider(new JacksonJsonProvider())
            .build();

    public static String getVersion(BuildProperties buildProperties) {
        return buildProperties.getArtifact() + " - " + buildProperties.getVersion();
    }

    public static void logRequest(Logger log, HttpServletRequest request) {
        if (log.isTraceEnabled()) {
            String headers = httpServletRequestHeadersToString(request);

            log.trace("Request: {}:{}[{}] -> {}:{}: headers: [{}], body.length: {}", request.getRemoteAddr(), request.getRemotePort(),
                      request.getRemoteUser(), request.getMethod(), request.getContextPath(), headers, request.getContentLengthLong());
        }
    }

    public static String httpServletRequestHeadersToString(HttpServletRequest request) {
        int headersCount = 0;
        StringBuilder headers = new StringBuilder();
        Enumeration<String> headerNames = request.getHeaderNames();

        headers.append("{");

        while (headerNames.hasMoreElements()) {
            if (headersCount > 0) {
                headers.append(", ");
            }
            headersCount++;
            String hdrName = headerNames.nextElement();
            String hdrValue = request.getHeader(hdrName);

            headers.append(hdrName).append("=").append(hdrValue);
        }
        headers.append("}");
        return headers.toString();
    }

    public static String httpHeadersToString(HttpHeaders httpHeaders) {
        return httpHeaders.toSingleValueMap().toString();
    }

    public static void logRequest(Logger log, HttpRequest request) {
        if (log.isTraceEnabled()) {
            String headers = httpHeadersToString(request.getHeaders());
            log.trace("Request[{}]: {} -> {}: headers: [{}],", request.hashCode(), request.getMethod(), request.getURI(), headers);
        }
    }

    public static String logResponseEntityToString(ResponseEntity<String> response) {
        return response.getStatusCodeValue() + ": '" + response.getBody() + "'";
    }
    
    public static boolean checkMapsAreIdentical(Map<String,Object> map1, Map<String,Object> map2) throws Exception {
        return Maps.difference(map1, map2).areEqual();
    }
}
