package com.spw.crudtestf3;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import com.jayway.jsonpath.JsonPath;
import com.spw.crudtestf3.controller.RecordController;
import com.spw.crudtestf3.domain.Record;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(DataLoader.class);

    @Autowired
    private RecordController recordController;

    @Override
    public void run(String... strings) throws Exception {
        try {
            //SecurityContextHolder.getContext().setAuthentication(WebSecurityConfig.getAuthenticationAdmin());

            loadDefaultRecords();

            // SecurityContextHolder.clearContext();
        } catch (Exception ex) {
            log.error("Problem loading data: {}", ExceptionUtils.getStackTrace(ex));
            throw ex;
        }
    }

    private void loadDefaultRecords() throws Exception {
        String resourcePath = "/data/41ca3269-d8c4-4063-9fd5-f306814ff03f.json"; // From: http://mockbin.org/bin/41ca3269-d8c4-4063-9fd5-f306814ff03f

        log.info("Loading default records from: {}", resourcePath);
        try (InputStream inputStream = this.getClass().getResourceAsStream(resourcePath)) {
            loadRecords(recordController, inputStream);
        }
    }

    public static void loadRecords(RecordController recordController, InputStream inputStream) throws IOException {

        long loadCount = 0L;

        List<Record> records = JsonPath.using(Util.JSONPATH_CONF).parse(inputStream, StandardCharsets.UTF_8.name()).read("$.data", Util.JSONPATH_TYPEREF_LIST);

        for (Record record : records) {
            if (log.isTraceEnabled()) {
                log.trace("Loading: {}", record);
            }

            recordController.postRecord(record);
            loadCount++;
        }
        log.trace("Loaded default records: {}", loadCount);
    }
}
