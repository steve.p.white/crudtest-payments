package com.spw.crudtestf3.repositories;

import com.spw.crudtestf3.domain.Record;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RestResource;

public interface RecordRepository extends MongoRepository<Record, String> {

    @Override
    default Page<Record> findAll(Pageable p) {
        if (p.getSort() == null) {   
            return findBy(new PageRequest(p.getPageNumber(), p.getPageSize(), Sort.Direction.DESC, "timeAdded")); // default sort order: timeAdded descending
        }
        return findBy(p);
    }
    
    @RestResource(exported = false)
    Page<Record> findBy(Pageable pageable);

}