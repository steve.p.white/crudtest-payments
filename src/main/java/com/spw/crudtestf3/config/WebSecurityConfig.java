package com.spw.crudtestf3.config;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String ADMIN_USER = "admin";
    public static final String ADMIN_PASS = new String(Base64.getDecoder().decode("Y3J1ZHRlc3RmMw=="), StandardCharsets.UTF_8); // simple obfuscation...
    
    private static final Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);
    
    static final String[] ADMIN_ROLES = new String[] {
            "ADMIN",
            "ACTUATOR" }; // allows access health/metrics

    public static Authentication getAuthenticationAdmin() {
        return new UsernamePasswordAuthenticationToken(ADMIN_USER, ADMIN_PASS,
                                                       AuthorityUtils.createAuthorityList(ADMIN_ROLES));
    }

    @Value(value = "${api.authdisable}")
    private Boolean authDisable;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser(ADMIN_USER).password(ADMIN_PASS).roles(ADMIN_ROLES);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers()
                .frameOptions()
                .sameOrigin(); // Disable X-Frame-Options header for same origin requests

        if (authDisable) {
            log.warn("Authentication disabled");
            http.authorizeRequests().anyRequest()
                    .permitAll().and()
                    .csrf().disable()
                    .logout()
                    .logoutSuccessUrl("/");
        } else {
            http.authorizeRequests()
                    .antMatchers("/info").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .defaultSuccessUrl("/", true)
                    .permitAll()
                    .and()
                    .httpBasic()
                    .and()
                    .csrf().disable()
                    .logout()
                    .logoutSuccessUrl("/");
        }
    }

}
