package com.spw.crudtestf3.config;

import com.spw.crudtestf3.domain.Record;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;


@Configuration
public class RepositoryRestMvcConfig 
 extends RepositoryRestMvcConfiguration {
//
    /**
     * Default Spring MVC REST API page size
     */
    public static final int DEFAULT_PAGE_SIZE = 3;
    
    @Override
    public RepositoryRestConfiguration config() {
        RepositoryRestConfiguration config = super.config();       
        config.exposeIdsFor(Record.class);
        config.setDefaultPageSize(DEFAULT_PAGE_SIZE);
        config.setBasePath("/api");
        return config;
    }
}