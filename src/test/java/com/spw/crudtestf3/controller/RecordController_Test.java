package com.spw.crudtestf3.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.jayway.jsonpath.JsonPath;
import com.spw.crudtestf3.ResourceTestSupport;
import com.spw.crudtestf3.TestUtil;
import com.spw.crudtestf3.Util;
import com.spw.crudtestf3.client.ApiParams;
import com.spw.crudtestf3.client.HalConstants;
import com.spw.crudtestf3.client.domain.HalPage;
import com.spw.crudtestf3.client.domain.HalResponse;
import com.spw.crudtestf3.client.domain.RecordJson;
import com.spw.crudtestf3.domain.Currency;
import com.spw.crudtestf3.domain.PaymentRecord_Test;
import com.spw.crudtestf3.domain.Record;
import com.spw.crudtestf3.domain.payment.CurrencyAmount;
import com.spw.crudtestf3.domain.payment.Party;
import com.spw.crudtestf3.domain.payment.Payment;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.NestedServletException;

/**
 * Test the CRUD API for the Records domain class
 * Each test performs a call to Via a REST call and validates the output
 * Certain tests also perform the same call to the MVC framework directly (no web container)
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@AutoConfigureMockMvc
public class RecordController_Test extends ResourceTestSupport {

    private static final Logger log = LoggerFactory.getLogger(RecordController_Test.class);

    @Rule
    public ExternalResource resource = new ExternalResource() {
        @Override
        protected void before() throws Exception {
            deleteAllRecords();
            loadDefaultPayments();
        }

        @Override
        protected void after() {
            try {
                deleteAllRecords();
            } catch (Exception e) {
                fail();
            }
        }
    };

    public RecordController_Test() {
        super(Record.class); // set the entity under test
    }

    public void loadDefaultPayments() throws Exception {
        String resourcePath = "/data/41ca3269-d8c4-4063-9fd5-f306814ff03f.json"; // From: http://mockbin.org/bin/41ca3269-d8c4-4063-9fd5-f306814ff03f

        log.info("Loading default payments from: {}", resourcePath);
        try (InputStream inputStream = this.getClass().getResourceAsStream(resourcePath)) {
            loadRecords(inputStream);
        }
    }

    @Test
    public void test_testBasicAuthEnforced() {
        validateBasicAuthOnEntityRestApi();
    }

    @Test
    public void test_postBadNonEntityData_throwsError() throws JsonParseException, JsonMappingException, IOException, Exception {
        String invalidData = "invalidData...";
        MvcResult result = performEntityMvcPost(invalidData); // try bad POST via MVC call
        assertThat(result.getResponse().getStatus(), equalTo(HttpStatus.BAD_REQUEST.value())); // fail due to non-JSON data

        ResponseEntity<String> resultRest = performEntityRestPost(invalidData); // try bad POST via REST call
        assertThat(resultRest.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        invalidData = "{ \"key\" : \"invalidData...\" }"; // try bad JSON POST via MVC
        try {
            performEntityMvcPost(invalidData);
            fail();
        } catch (NestedServletException nse) {
            log.error("CaughtException: {}" + ExceptionUtils.getMessage(nse));
            assertThat(nse.getCause(), instanceOf(IllegalArgumentException.class));
        }

        resultRest = performEntityRestPost(invalidData); // try bad JSON POST via REST
        assertThat(resultRest.getStatusCode(), equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * Get all records (a single page worth). This will always be a minimum of 3 records regardless of test order
     */
    @Test
    public void test_getAllRecordsFirstPage() throws Exception {

        List<Record> resultsMvc = getEntityResults(performEntityMvcGet(), Util.JSONPATH_TYPEREF_LIST);
        assertThat(resultsMvc.size(), greaterThanOrEqualTo(3));

        List<Record> resultsRest = getEntityResults(performEntityRestGet(), Util.JSONPATH_TYPEREF_LIST);
        assertThat(resultsRest.size(), greaterThanOrEqualTo(3));
    }

    @Test
    public void test_getFirstRecord() throws Exception {

        List<Record> records = new ArrayList<>();

        String recordId = "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43";
        records.add(getEntityResult(performEntityMvcGet("/" + recordId), Record.class)); // get via MVC
        records.add(getEntityResult(performEntityRestGet("/" + recordId), Record.class)); // get via REST

        records.forEach(record -> {
            // perform some a basic validation of the record
            assertThat(record.getTimeUpdated(), greaterThan(timeBefore));
            assertThat(record.getType(), equalTo("Payment"));
            assertThat(record.getId(), equalTo("4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43"));
            assertThat(record.getOrganisation_id(), equalTo("743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb"));
            Payment payment = (Payment) RecordController.getInnerRecord(record);

            assertThat(payment.getAmount(), equalTo("100.21"));

            Party beneficiary_party = payment.getBeneficiary_party();
            assertThat(beneficiary_party.getAccount_name(), equalTo("W Owens"));

            assertThat(beneficiary_party.getAccount_number(), equalTo("31926819"));

            List<CurrencyAmount> senderCharges = payment.getCharges_information().getSender_charges();
            assertThat(senderCharges.size(), equalTo(2));

            assertThat(senderCharges.get(1).getAmount(), equalTo("10.00"));
            assertThat(senderCharges.get(1).getCurrency(), equalTo(Currency.USD));

            assertThat(payment.getCurrency(), equalTo(Currency.GBP));

        });
    }

    @Test
    public void test_getInvalidRecord() throws Exception {
        MvcResult result = performEntityMvcGet("/9999999999");
        assertThat(result.getResponse().getStatus(), equalTo(HttpStatus.NOT_FOUND.value()));

        ResponseEntity<String> resultRest = performEntityRestGet("/9999999999");
        assertThat(resultRest.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    /**
     * Invoked by other test methods to post a record via MVC & REST methods. Each:
     * - Validate returned record is correct
     * - Explicitly get the record and validate it's correct
     * - Validate the timeAdded/timeUpdated fields
     */
    public String postRecordTestsOk(Record record) throws Exception {
        Long timeAdded = record.getTimeAdded();
        Long timeUpdated = record.getTimeUpdated();
        String recordJson = getEntityJson(record);

        // MVC test
        MvcResult mvcResult = performEntityMvcPost(recordJson);
        assertThat(mvcResult.getResponse().getStatus(), equalTo(HttpStatus.CREATED.value()));

        Record recordOut = getEntityResult(mvcResult, Record.class);
        assertDataEquals(recordOut, record); // ensure the data sent back from the post reflects the data sent in

        String recordOutMvcId = getRecordIdFromResult(mvcResult);

        Record recordOutGet = getEntityResult(performEntityMvcGet("/" + recordOutMvcId), Record.class); // read the record again from the API

        assertThat(recordOutGet.equalsData(record), equalTo(true));
        assertThat(recordOutGet.getTimeAdded(), not(equalTo(timeAdded))); // any timeAdded/Updated values sent in should be overridden by the API 
        assertThat(recordOutGet.getTimeUpdated(), not(equalTo(timeUpdated)));

        // REST test
        ResponseEntity<String> restResult = performEntityRestPost(recordJson);
        assertThat(restResult.getStatusCode(), equalTo(HttpStatus.CREATED));

        recordOut = getEntityResult(restResult, Record.class); // read the record again from the API
        assertThat(recordOut.equalsData(record), equalTo(true)); // ensure the data sent back from the post reflects the data sent in
        String recordOutRestId = getRecordIdFromResult(restResult);

        recordOutGet = getEntityResult(performEntityRestGet("/" + recordOutRestId), Record.class);
        assertThat(recordOutGet.equalsData(record), equalTo(true)); // ensure that the returned data is equal to what was sent in

        assertThat(recordOutRestId, equalTo(recordOutMvcId)); // check that the record id remains the same
        assertThat(recordOutGet.getTimeAdded(), not(equalTo(timeAdded))); // any timeAdded/Updated values sent in should be overridden by the API 
        assertThat(recordOutGet.getTimeUpdated(), not(equalTo(timeUpdated)));

        return recordOutRestId;
    }

    /**
     * Create a payment record - simple test
     */
    @Test
    public void test_postRecord() throws JsonParseException, JsonMappingException, IOException, Exception {
        Record record = PaymentRecord_Test.getBaseTestPaymentRecord();
        postRecordTestsOk(record);
    }

    /**
     * Post a record containing unicode characters
     */
    @Test
    public void test_postRecordUnicode() throws JsonParseException, JsonMappingException, IOException, Exception {
        Record record = PaymentRecord_Test.getBaseTestPaymentRecord();
        record.setValue("attributes/debtor_party/name", "JoƐ Andrew BloggṠ");

        postRecordTestsOk(record);
    }

    @Test
    public void test_postRecord_invalidData_long() throws JsonParseException, JsonMappingException, IOException, Exception {
        // test max field length for debtor name = 32

        Record record = PaymentRecord_Test.getBaseTestPaymentRecord();
        record.setValue("attributes/debtor_party/name", "JoeJoeJoeJoeJoeJoeJoeJoeJoeJoeJoeJoeJoe BloggsBloggsBloggsBloggsBloggsBloggs");

        postRecordTestsInvalid(record);
    }

    @Test
    public void test_postRecord_invalidData_short() throws JsonParseException, JsonMappingException, IOException, Exception {
        // test minimum field lengths

        Record record = PaymentRecord_Test.getBaseTestPaymentRecord();
        record.setValue("organisation_id", "");

        postRecordTestsInvalid(record);

        record = PaymentRecord_Test.getBaseTestPaymentRecord();
        record.setValue("attributes/debtor_party/name", "");

        postRecordTestsInvalid(record);

        record = PaymentRecord_Test.getBaseTestPaymentRecord();
        record.setValue("attributes/amount", "");

        postRecordTestsInvalid(record);

        record = PaymentRecord_Test.getBaseTestPaymentRecord();
        record.setValue("attributes/payment_id", "");

        postRecordTestsInvalid(record);
    }

    @Test
    public void test_putBadRecord() throws JsonParseException, JsonMappingException, IOException {
        Record record = PaymentRecord_Test.getBaseTestPaymentRecord();
        record.setId("8888888");
        ResponseEntity<String> result = performEntityRestPut("8888888", getEntityJson(record));
        assertThat(result.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

   
    @Test
    public void test_putPayment() throws Exception {

        Record record = PaymentRecord_Test.getBaseTestPaymentRecord();

        // add a record and get it's ID from the response
        ResponseEntity<String> result = performEntityRestPost(getEntityJson(record));
        String recordId = getRecordIdFromResult(result);

        // ensure record is there and the data is the same as what we sent in
        ResponseEntity<String> resultGet = performEntityRestGet("/" + recordId);
        assertThat(resultGet.getStatusCode(), equalTo(HttpStatus.OK));

        Record recordGet = getEntityResult(resultGet, Record.class);
        assertThat(recordGet.equalsData(record), equalTo(true));

        Long timeLastUpdated = recordGet.getTimeUpdated();

        // update the same record with a different amount
        Record recordNew = PaymentRecord_Test.getBaseTestPaymentRecord();
        recordNew.getAttributes().put("amount", "333444.00");

        result = performEntityRestPut(recordId, getEntityJson(recordNew));
        assertThat(result.getStatusCode(), equalTo(HttpStatus.CREATED));

        String recordNewId = getRecordIdFromResult(result);

        assertThat(recordNewId, equalTo(recordId)); // check the id in the response and ensure it hasn't created a new record

        // ensure the record has been updated and the timeUpdated field has increased
        Record recordNewGet = getEntityResult(result, Record.class);
        assertThat(recordNewGet.equalsData(recordNew), equalTo(true));

        assertThat(recordNewGet.getTimeUpdated(), greaterThan(timeLastUpdated));

        // update the record again with different amount and beneficiary account name
        timeLastUpdated = recordGet.getTimeUpdated();
        recordNew = PaymentRecord_Test.getBaseTestPaymentRecord();
        recordNew.setValue("attributes/amount", "1.99");
        recordNew.setValue("attributes/beneficiary_party/account_name", "Dave Smith");

        result = performEntityRestPut(recordId, getEntityJson(recordNew));
        assertThat(result.getStatusCode(), equalTo(HttpStatus.CREATED));

        recordNewId = getRecordIdFromResult(result);
        assertThat(recordNewId, equalTo(recordId)); // check the id still hasn't changed

        recordNewGet = getEntityResult(result, Record.class);
        assertThat(recordNewGet.equalsData(recordNew), equalTo(true)); // check the response contains the data we sent in 

        assertThat(recordNewGet.getTimeUpdated(), greaterThan(timeLastUpdated)); // check that the timeUpdated field has increased

        // update the same record with invalid data - no beneficiary party
        Record recordInvalid1 = PaymentRecord_Test.getBaseTestPaymentRecord();
        recordInvalid1.setValue("attributes/beneficiary_party/account_name", null);

        result = performEntityRestPut(recordId,  getEntityJson(recordInvalid1));
        validateRestConstraintError(result);

        // update the same record with invalid data (multiple constraint problems) - payment_purpose too short (min 1)
        recordInvalid1.setValue("attributes/beneficiary_party/payment_purpose", null);

        result = performEntityRestPut(recordId, getEntityJson(recordInvalid1));
        validateRestConstraintError(result);
        
        // check the record hasn't changed from the last successful update
        resultGet = performEntityRestGet("/" + recordId);
        assertThat(resultGet.getStatusCode(), equalTo(HttpStatus.OK));

        recordGet = getEntityResult(resultGet, Record.class);
        assertDataEquals(recordNew, recordGet);
    }

    @Test
    public void test_getRecordsPageSize() throws Exception {
       
        int pageSize=1;
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
        
        params.add(ApiParams.SIZE, "" + pageSize);

        ResponseEntity<String> response = performEntityRestGet(null, params);
        List<Record> results = getEntityResults(response, Util.JSONPATH_TYPEREF_LIST);
        assertThat(results.size(), equalTo(pageSize));

        pageSize=3;
        params = new LinkedMultiValueMap<String, String>();
        params.add(ApiParams.SIZE, "" + pageSize);

        response = performEntityRestGet(null, params);
        results = getEntityResults(response, Util.JSONPATH_TYPEREF_LIST);

        assertThat(results.size(), equalTo(pageSize));
    }

    @Test
    public void test_getRecordsPaging() throws Exception {
        long testPageSize = 10;
        long totalRecords = 100;
        
        long pages = totalRecords/testPageSize;

        List<RecordJson<? extends Record>> newRecords = TestUtil.generateTestRecords(PaymentRecord_Test.getBaseTestPaymentRecord(), totalRecords);
        performTestDataRestPost(newRecords);

        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
        params.add(ApiParams.SIZE, "" + testPageSize); // force page size limit
        ResponseEntity<String> response = performEntityRestGet(null, params);

        HalResponse halResponse = getEntityResult(response, HalResponse.class);

        HalPage page = halResponse.getPage();

        assertThat(page.getSize(), equalTo(testPageSize));
        assertThat(page.getTotalElements(), greaterThanOrEqualTo(totalRecords));
        assertThat(page.getTotalPages(), greaterThanOrEqualTo(pages));
        assertThat(page.getNumber(), equalTo(0L));

        // convert _embedded -> ...
        List<Record> records = getJacksonMapper().convertValue(halResponse.get_embedded().get(getResourceName()), Util.JACKSON_TYPEREF_LIST);

        assertThat(records.size(), equalTo((int)testPageSize)); // validate page size
    }

    @Test
    public void test_validateHalLinks() throws Exception {
        ResponseEntity<String> response = performEntityRestGet();

        // Check _embedded -> _links -> self -> href = http://localhost:8080/api/records{?page,size,sort}"
        String embeddedSelfHref = JsonPath.parse(response.getBody(), Util.JSONPATH_CONF).read("$._links.self.href", String.class);
        String supportedParams = ApiParams.getSupportedParamsValue();
        String expectedSelfPath = getUriBase() + getEntityUriPath(null) + "{?" + supportedParams + "}";

        assertThat(embeddedSelfHref, equalTo(expectedSelfPath));

        // Check _embedded -> _links -> profile -> href = http://localhost:8080/api/profile/records
        String embeddedProfileHref = JsonPath.parse(response.getBody(), Util.JSONPATH_CONF).read("$._links.profile.href", String.class);
        String expectedProfileHref = getUriBase() + API_PREFIX + "/" + HalConstants.PROFILE + "/" + getResourceName();

        assertThat(embeddedProfileHref, equalTo(expectedProfileHref));
    }


    @Test
    public void test_validateEntityLinks() throws Exception {
        String id = "ab4bbd28-33c6-4231-9b64-0e96190f59ef";
        ResponseEntity<String> response = performEntityRestGet("/" + id);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        validateEntityLinksForRecord(response, id);
    }

    /**
     * POST 1k records to the API using a number of send threads.
     * Query all the records page by page (checking page links) and validate all sent records they're there and correct.
     */
    @Test
    public void test_postRecords1k_parallel() throws Exception {

        int totalRecords = 1000;
        int sendThreads = DEFAULT_POST_THREADS;

        List<RecordJson<? extends Record>> newRecords = TestUtil.generateTestRecords(PaymentRecord_Test.getBaseTestPaymentRecord(), totalRecords);
        performTestDataRestPost(newRecords);

        final Map<String, RecordJson<? extends Record>> recordsIdx = new ConcurrentHashMap<String, RecordJson<? extends Record>>();

        // - POST 1k records to the API using a number of send threads. 
        // - Create an index of the returned IDs
        // - Assert that it took less than 60s 
        // Any reasonable build server, even under load, should be able to insert this number of records in under 60s
        postRecordsInParallel(sendThreads, newRecords, recordsIdx, Record.class, 60000L);

        // Now get all records and validate them...
        ResponseEntity<String> response = performEntityRestGet();

        // - Process API page response validating the data output with data originally sent in
        // - Validate page links
        // - Follow page links recursively

        processRestEntityGetPages(response, recordsIdx, Util.JSONPATH_TYPEREF_LIST);

        // check that all the records we sent in were seen and validated
        for (RecordJson<? extends Record> record : newRecords) {
            assertThat(record.getSeen(), equalTo(true));
        }
    }

    /**
     * Used by tests to post and validate the response for an event known to violate constraints
     * This tests both the MVC and REST API's
     */
    public void postRecordTestsInvalid(Record record) throws Exception {
        try {
            // MVC should throw a constraint exception
            performEntityMvcPost(getEntityJson(record));
            fail();
        } catch (NestedServletException nse) {
            log.error("CaughtException: {}" + ExceptionUtils.getMessage(nse));
            assertThat(nse.getCause(), instanceOf(DataIntegrityViolationException.class));
        }

        // REST should return a JSON error result
        ResponseEntity<String> restResponse = performEntityRestPost(getEntityJson(record));
        validateRestConstraintError(restResponse);
    }
    
    @Test
    public void test_deleteRecord() throws Exception {
        Record newRecord = PaymentRecord_Test.getBaseTestPaymentRecord();
        newRecord.setId("d00b06fa-6cda-4f68-9556-6ad627fcdd0e");

        String recordId = postRecordTestsOk(newRecord); // create a record and get the record Id, before removing

        ResponseEntity<String> deleteResponse = performEntityRestDelete("/" + recordId);
        assertThat(deleteResponse.getStatusCode(), equalTo(HttpStatus.NO_CONTENT)); // 204

        deleteResponse = performEntityRestDelete("/888888888");
        assertThat(deleteResponse.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

        ResponseEntity<String> recordResponse = performEntityRestGet("/" + recordId); // validate it has been deleted
        assertThat(recordResponse.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    public void deleteAllRecords() throws Exception {
        ResponseEntity<String> deleteResponse = performEntityRestDelete("/");
        assertThat(deleteResponse.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

        ResponseEntity<String> getResponse = performEntityRestGet();
        HalResponse halResponse = getEntityResult(getResponse, HalResponse.class);
        assertThat(halResponse.getPage().getTotalElements(), equalTo(0L));
    }
}
