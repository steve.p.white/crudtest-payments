package com.spw.crudtestf3.client;

public class HalConstants {
    public static final String HREF = "href";
    public static final String PROFILE = "profile";
    public static final String EMBEDDED = "_embedded";
}
