package com.spw.crudtestf3.client.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Builder
public class RecordJson<T> {
    String recordJson;
    T record;
    Boolean seen;

    public RecordJson(T record, String recordJson) {
        this.record = record;
        this.recordJson = recordJson;
    }
}
