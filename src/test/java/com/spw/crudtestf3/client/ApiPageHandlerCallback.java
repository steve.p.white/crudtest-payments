package com.spw.crudtestf3.client;

import org.springframework.http.ResponseEntity;

public interface ApiPageHandlerCallback {

    public String processPage(ResponseEntity<String> response) throws Exception;
}
