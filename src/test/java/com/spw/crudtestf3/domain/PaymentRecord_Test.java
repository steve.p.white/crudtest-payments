package com.spw.crudtestf3.domain;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import com.spw.crudtestf3.controller.RecordController;
import com.spw.crudtestf3.domain.payment.ChargesInformation;
import com.spw.crudtestf3.domain.payment.CurrencyAmount;
import com.spw.crudtestf3.domain.payment.Fx;
import com.spw.crudtestf3.domain.payment.Party;
import com.spw.crudtestf3.domain.payment.Payment;
import com.spw.crudtestf3.domain.payment.SponsorParty;
import org.apache.commons.lang3.SerializationUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;

public class PaymentRecord_Test {
    private static final Logger log = LoggerFactory.getLogger(PaymentRecord_Test.class);

    @Test
    public void test_compareObjects() throws Exception {
        // test pojo hashcode/equals

        Record paymentRecord1 = getBaseTestPaymentRecord();

        assertThat(paymentRecord1.equalsRecord(null), equalTo(false));
        assertThat(paymentRecord1.equalsRecord(""), equalTo(false));
        assertThat(paymentRecord1.equalsRecord(paymentRecord1), equalTo(true)); // Test all fields are equal i.e metadata (time* fields) and payment data

        assertThat(paymentRecord1.equalsData(null), equalTo(false));
        assertThat(paymentRecord1.equalsData(""), equalTo(false));
        assertThat(paymentRecord1.equalsData(paymentRecord1), equalTo(true)); // Test all data fields only, are equal

        Record paymentRecord2 = Record.builder() // different beneficiary from paymentRecord1 
                .type(RecordTypes.PAYMENT)
                .id("123e60ba-1334-439f-91db-32cc3cde036a")
                .version(0L)
                .timeAdded(999999999L)
                .organisation_id("123d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb")
                .attributes(Payment.builder()
                        .amount("1234.80")
                        .beneficiary_party(Party.builder()
                                .account_name("S White")
                                .account_number("123456789")
                                .account_number_code("BBAN")
                                .account_type(0)
                                .address("123 My Street")
                                .bank_id("9999")
                                .bank_id_code("UBS")
                                .name("Steve White")
                                .build())
                        .charges_information(ChargesInformation.builder()
                                .receiver_charges_amount("3.12")
                                .receiver_charges_currency(Currency.GBP)
                                .bearer_code("SHAR")
                                .sender_charges(Lists.newArrayList(CurrencyAmount.builder()
                                        .amount("6.00")
                                        .currency(Currency.GBP)
                                        .build()))
                                .build())
                        .currency(Currency.GBP)
                        .debtor_party(Party.builder()
                                .account_name("DA Smith")
                                .account_number("GB29XABC10161234567801")
                                .account_number_code("IBAN")
                                .address("10 Debtor Crescent Sourcetown NE1")
                                .bank_id("4444444")
                                .bank_id_code("UBS")
                                .name("Dave Andrew Smith")
                                .build())
                        .end_to_end_reference("Wil piano Jan")
                        .fx(Fx.builder()
                                .contract_reference("FX123")
                                .exchange_rate("2.00000")
                                .original_amount("200.42")
                                .original_currency(Currency.USD)
                                .build())
                        .numeric_reference("111111111")
                        .payment_id("123456789012345678")
                        .payment_scheme("FPS")
                        .payment_type("Credit")
                        .processing_date("2018-01-30")
                        .reference("Payment for a holiday")
                        .scheme_payment_sub_type("InternetBanking")
                        .scheme_payment_type("ImmediatePayment")
                        .sponsor_party(SponsorParty.builder()
                                .account_number("56781235")
                                .bank_id("00002")
                                .bank_id_code("GBDSC")
                                .build())
                        .build().toMap())
                .build();

        Record paymentRecord3 = SerializationUtils.clone(paymentRecord1);
        paymentRecord3.timeAdded = 123456L;

        assertThat(paymentRecord1.equalsData(paymentRecord3), equalTo(true)); // same data, but meta differs

        assertThat(paymentRecord1.equalsData(paymentRecord2), equalTo(false)); // data differs (payment beneficiary)

        paymentRecord3.setId("323e60ba-1334-439f-91db-32cc3cde036a");

        assertThat(paymentRecord1.equalsRecord(paymentRecord3), equalTo(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_bad_RecordType() throws Exception {
        Record basePaymentRecord = getBaseTestPaymentRecord();
        basePaymentRecord.type = "badType";

        RecordController.validateRecord(basePaymentRecord);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void test_bad_beneficiaryParty_name() throws Exception {
        Record basePaymentRecord = getBaseTestPaymentRecord();
        basePaymentRecord.setValue("/attributes/beneficiary_party/name", "");

        RecordController.validateRecord(basePaymentRecord);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void test_bad_charges_information_receiver_charges_amount() throws Exception {
        Record basePaymentRecord = getBaseTestPaymentRecord();
        basePaymentRecord.setValue("/attributes/charges_information/receiver_charges_amount", null);

        RecordController.validateRecord(basePaymentRecord);
    }

    public static Record getBaseTestPaymentRecord() {
        return Record.builder()
                .type("Payment")
                .id("123e60ba-1334-439f-91db-32cc3cde036a")
                .version(0L)
                .timeAdded(666666666L)
                .organisation_id("123d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb")
                .attributes(Payment.builder()
                        .amount("1234.80")
                        .beneficiary_party(Party.builder()
                                .account_name("J Bloggs")
                                .account_number("123456789")
                                .account_number_code("BBAN")
                                .account_type(0)
                                .address("123 My Street")
                                .bank_id("9999")
                                .bank_id_code("UBS")
                                .name("Joe Bloggs")
                                .build())
                        .charges_information(ChargesInformation.builder()
                                .receiver_charges_amount("2.56")
                                .receiver_charges_currency(Currency.GBP)
                                .bearer_code("SHAR")
                                .sender_charges(Lists.newArrayList(CurrencyAmount.builder()
                                        .amount("6.00")
                                        .currency(Currency.GBP)
                                        .build()))
                                .build())
                        .currency(Currency.GBP)
                        .debtor_party(Party.builder()
                                .account_name("DA Smith")
                                .account_number("GB29XABC10161234567801")
                                .account_number_code("IBAN")
                                .address("10 Debtor Crescent Sourcetown NE1")
                                .bank_id("4444444")
                                .bank_id_code("UBS")
                                .name("Dave Andrew Smith")
                                .build())
                        .end_to_end_reference("Wil piano Jan")
                        .fx(Fx.builder()
                                .contract_reference("FX123")
                                .exchange_rate("2.00000")
                                .original_amount("200.42")
                                .original_currency(Currency.USD)
                                .build())
                        .numeric_reference("111111111")
                        .payment_id("123456789012345678")
                        .payment_scheme("FPS")
                        .payment_type("Credit")
                        .payment_purpose("Paying for services")
                        .processing_date("2018-01-30")
                        .reference("Payment for a holiday")
                        .scheme_payment_sub_type("InternetBanking")
                        .scheme_payment_type("ImmediatePayment")
                        .sponsor_party(SponsorParty.builder()
                                .account_number("56781235")
                                .bank_id("00002")
                                .bank_id_code("GBDSC")
                                .build())
                        .build().toMap())
                .build();
    }
}
