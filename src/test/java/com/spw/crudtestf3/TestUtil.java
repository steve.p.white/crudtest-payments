package com.spw.crudtestf3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spw.crudtestf3.client.domain.RecordJson;
import com.spw.crudtestf3.domain.Record;
import org.apache.commons.lang3.SerializationUtils;

public class TestUtil {

    /**
     * Generate a number of test records using an existing record as a template. This modifies each field to include a count prefix with the current record number.
     * 
     * @param baseRecord the template record
     * @param count number of records to generate
     */
    public static List<RecordJson<? extends Record>> generateTestRecords(Record baseRecord, long count) throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        List<RecordJson<? extends Record>> newRecords = new ArrayList<>();
        // Get a base payment record and modify each field to include a count value, to make it completely unique
        for (long i = 0; i < count; i++) {
            Record newRecord = SerializationUtils.clone(baseRecord);
            newRecord.setId("" + UUID.randomUUID()); // set a new id

            String prefix = "" + i;
            prefixMapValues(newRecord.getAttributes(), prefix);

            // pre-generate JSON to send to the API so we can send quickly for load tests
            String json = mapper.writeValueAsString(newRecord);

            newRecords.add(new RecordJson<Record>(newRecord, json));
        }
        return newRecords;
    }

    private static void prefixMapValues(Map<String, Object> map, String prefix) throws Exception {
        if (map == null) {
            return;
        }

        if (map.size() == 0) {
            return;
        }

        Iterator<String> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            String keyLower = key.toLowerCase();
            // TODO: Quick hack to prevent us from altering special types. We can do better...
            if (!keyLower.contains("currency") && 
                    !keyLower.contains("date") && 
                    !keyLower.contains("code")) { 
                Object val = map.get(key);
                if (val != null) {
                    Class<?> cls = val.getClass();
                    if (isPrimitiveClass(cls)) {
                        map.put(key, prefixValue(val, prefix));
                    } else {
                        prefixValueType(cls, val, prefix);
                    }
                }
            }
        }
    }

    private static Object prefixValue(Object val, String prefix) {
        StringBuilder str = new StringBuilder();
        str.append(prefix).append(val).append(prefix);

        return str.toString();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void prefixValueType(Class<?> cls, Object val, String prefix) throws Exception {
        if (Map.class.isAssignableFrom(cls)) {
            prefixMapValues((Map) val, prefix);
        } else if (List.class.isAssignableFrom(cls)) {
            prefixListValues((List<?>) val, prefix);
        } else {
            throw new Exception("Unhandled type: " + cls.getName());
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void prefixListValues(List list, String prefix) throws Exception {

        if (list == null) {
            return;
        }
        if (list.size() == 0) {
            return;
        }
        for (int i = 0; i < list.size(); i++) {
            Object val = list.get(i);
            if (val != null) {
                Class<?> cls = val.getClass();
                if (isPrimitiveClass(cls)) {
                    list.set(i, prefixValue(val, prefix));
                } else {
                    prefixValueType(cls, val, prefix);
                }
            }
        }
    }

    public static boolean isPrimitiveClass(Class<?> clazz) {
        return clazz.equals(String.class) ||
                clazz.equals(Boolean.class) ||
                clazz.equals(boolean.class) ||
                clazz.equals(Integer.class) ||
                clazz.equals(int.class) ||
                clazz.equals(Character.class) ||
                clazz.equals(char.class) ||
                clazz.equals(Long.class) ||
                clazz.equals(long.class) ||
                clazz.equals(Byte.class) ||
                clazz.equals(byte.class) ||
                clazz.equals(Short.class) ||
                clazz.equals(short.class) ||
                clazz.equals(Double.class) ||
                clazz.equals(double.class) ||
                clazz.equals(Float.class) ||
                clazz.equals(float.class);
    }
}
